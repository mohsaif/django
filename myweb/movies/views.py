from django.http import HttpResponse
from  movies.models import Movie
from django.template import loader


def index(request):
    all_movies = Movie.objects.all()
    template = loader.get_template('vector/index.html')
    context = {

        'all_movies': all_movies
    }

    return HttpResponse(template.render(context),request)

def detail(request,movies_id):
    return HttpResponse("<h1> This movie number :" + str(movies_id) + "</h1>")
