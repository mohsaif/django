# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Kaust_User(models.Model):
    user_name = models.CharField(max_length=50)
    kaust_id = models.IntegerField()

class KAUST_PI(Kaust_User):
    pocket_id = models.IntegerField()
    available_seats = models.IntegerField()
